# TODO

- Make quoted text more flexible:
    - Things like \*strong\* and $math$ could all be defined using special inlines that have "quotation" shortcuts.  For example,

    ```haskell
    data QuotedText m = 
        { _toMarkup :: Text -> Markup m
        , _quotationEncloser :: Maybe (Text, Text)
        }
    ```

    Then a language definition would include a `Vector` of these.

    - I think this is actually a terrible idea.  It violates the principle that idoc should be easy to understand, even if you haven't learned how to use it.

## Titles

- Titles have a prefix (ie "Theorem"), a number (ie "7"), and a "title body" (ie "Euler's Theorem")

- Need to have a template for these?  (Please no)

- Need short titles and ToC titles too.

## Numbering

- Relative, absolute, unnumbered for counters

- Can be overridden to unnumbered on a per instance basis

## Attrmaps

- Change to YAML-y JSON

- No blocks/inlines inside (unless you parse yourself)

## SetIDs (Rename to Anchor?)

- Should be generated for each object