{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
module Text.IDoc.Syntax where

import           Data.Data
import           Control.Lens
import Data.Vector
import Data.Text
import GHC.Generics

data ParaContent b i =
    ParaInline (Inline i)
  | ParaBlock (b i)
  deriving (Eq, Ord, Show, Data, Typeable, Generic)

data Inline i =
    Text Text
  | Inline i
  deriving (Eq, Ord, Show, Data, Typeable, Generic, Functor)

data Block b i =
    Block (b i)
  | ParagraphC (Paragraph b i)
  deriving (Eq, Ord, Show, Data, Typeable, Generic)

data Paragraph b i =
  Paragraph
    { _paraContents :: Vector (ParaContent b i)
    , _paraLabel :: Maybe Label
    } deriving (Eq, Ord, Show, Data, Typeable, Generic)

data Doc e b i =
  Doc
    { _docTitle :: Maybe Title
    , _docContents :: Vector (Block b i)
    , _docLabel :: Maybe Label
    , _docMetadata :: Maybe e
    } deriving (Eq, Ord, Show, Data, Typeable, Generic)

-- | An `ID' given to an object so that it can be referred to later.
newtype Label = Label { _unLabel :: Text }
  deriving (Eq, Ord, Show, Data, Typeable, Generic)

newtype Title = Title { _unTitle :: Text }
  deriving (Eq, Ord, Show, Data, Typeable, Generic)

newtype BlockType = BlockType { _unBlockType :: Text }
  deriving (Eq, Ord, Show, Data, Typeable, Generic)

newtype InlineType = InlineType { _unInlineType :: Text }
  deriving (Eq, Ord, Show, Data, Typeable, Generic)

data SectionType = SectionType { _unSectionType :: Text, _sectionLevel :: Int}
  deriving (Eq, Ord, Show, Data, Typeable, Generic)

makeLenses ''ParaContent
makeLenses ''Inline
makeLenses ''Block
makeLenses ''Paragraph
makeLenses ''Doc
makeLenses ''Label
makeLenses ''Title
makeLenses ''BlockType
makeLenses ''InlineType
makeLenses ''SectionType