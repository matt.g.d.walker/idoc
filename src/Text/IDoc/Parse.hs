{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
module Text.IDoc.Parse where

import qualified Text.Megaparsec               as MP
import           Data.Aeson
import           Control.Lens            hiding ( cons )
import qualified Data.Set as S
import qualified Data.Text                     as T
import qualified Data.Text.Lazy.Builder        as TB
import qualified Data.Text.Lazy                as TL
import qualified Data.Vector                   as V
import qualified Data.Foldable                 as F
import           Control.Monad
import           Control.Applicative
import           Control.Monad.Reader

import           Text.IDoc.Syntax

newtype IDocParser e b i a
  = IDocParser { _unIDocParser :: MP.ParsecT e T.Text (ReaderT (IDocReaderState (IDocParser e b i) b i) Identity) a }
    deriving (Functor, Applicative, Alternative, Monad, MonadReader (IDocReaderState (IDocParser e b i) b i))
deriving instance Ord e => MonadPlus (IDocParser e b i)
deriving instance Ord e => MP.MonadParsec e T.Text (IDocParser e b i)

data IDocReaderState m b i =
  IDocReaderState
    { _blocksP :: BlockType -> Maybe Label -> Maybe Title -> Maybe Value -> m (b i)
    , _sectionsP :: SectionType -> Maybe Label -> Maybe Title -> Maybe Value -> m (b i)
    , _inlinesP :: InlineType -> Maybe Label -> Maybe Value -> m i
    , _blockAttrsP :: m Value
    , _inlineAttrsP :: m Value
    }

makeLenses ''IDocReaderState

labelP :: MP.MonadParsec e T.Text m => m Label
labelP = MP.label "label" $ do
  void $ MP.single '<'
  label <- Label <$> MP.takeWhile1P (Just "Label") (/= '>')
  void $ MP.single '>'
  return label

-- * Blocks
blockLabelP :: MP.MonadParsec e T.Text m => m Label
blockLabelP = MP.label "block label" $ do
  label <- labelP
  void $ MP.single '\n'
  return label

blockTypeP :: MP.MonadParsec e T.Text m => m BlockType
blockTypeP = MP.label "block type" $ do
  void $ MP.single '@'
  blockType <- BlockType <$> MP.takeWhile1P (Just "block type") (/= '\n')
  void $ MP.single '\n'
  return blockType

blockTitleP :: MP.MonadParsec e T.Text m => m Title
blockTitleP = MP.label "block title" $ do
  void $ MP.single '#'
  blockTitle <- Title <$> MP.takeWhile1P (Just "block title") (/= '\n')
  void $ MP.single '\n'
  return blockTitle

blockP'
  :: (MP.MonadParsec e T.Text m, MonadReader (IDocReaderState m b i) m)
  => m (b i)
blockP' = MP.label "block" $ do
  blockType <- blockTypeP
  mLabel    <- optional blockLabelP
  mTitle    <- optional blockTitleP
  attrsP    <- view blockAttrsP
  mAttrs    <- optional $ do
    void $ MP.chunk "...\n"
    attrsP
  void $ MP.chunk "---\n"
  blocksP' <- view blocksP
  block    <- blocksP' blockType mLabel mTitle mAttrs
  void $ MP.chunk "---"
  return block

blockP
  :: (MP.MonadParsec e T.Text m, MonadReader (IDocReaderState m b i) m)
  => m (Block b i)
blockP = Block <$> blockP'

-- * Sections

sectionTypeP :: MP.MonadParsec e T.Text m => m SectionType
sectionTypeP = do
  level       <- T.length <$> MP.takeWhile1P Nothing (== '=')
  sectionType <- MP.takeWhile1P Nothing (/= '\n')
  void $ MP.single '\n'
  return $ SectionType sectionType level

sectionP'
  :: (MP.MonadParsec e T.Text m, MonadReader (IDocReaderState m b i) m)
  => m (b i)
sectionP' = do
  sectionType <- sectionTypeP
  mLabel      <- optional blockLabelP
  mTitle      <- optional blockTitleP
  attrsP      <- view blockAttrsP
  mAttrs      <- optional $ do
    void $ MP.chunk "...\n"
    attrsP
  void $ MP.chunk "---\n"
  sectionsP' <- view sectionsP
  sectionsP' sectionType mLabel mTitle mAttrs

sectionP
  :: (MP.MonadParsec e T.Text m, MonadReader (IDocReaderState m b i) m)
  => m (Block b i)
sectionP = Block <$> sectionP'

-- * Inlines
inlineTypeP :: MP.MonadParsec e T.Text m => m InlineType
inlineTypeP = MP.label "inline type" $ do
  void $ MP.single '%'
  InlineType <$> MP.takeWhile1P Nothing (`notElem` ("<{[\n " :: String))

inlineP'
  :: (MP.MonadParsec e T.Text m, MonadReader (IDocReaderState m b i) m) => m i
inlineP' = MP.label "inline" $ do
  inlineType <- inlineTypeP
  mLabel     <- optional labelP
  attrsP     <- view inlineAttrsP
  mAttrs     <- optional $ do
    void $ MP.single '{'
    attrs <- attrsP
    void $ MP.single '}'
    return attrs
  inlinesP' <- view inlinesP
  void $ MP.single '['
  inline <- inlinesP' inlineType mLabel mAttrs
  void $ MP.single ']'
  return inline

inlineP
  :: (MP.MonadParsec e T.Text m, MonadReader (IDocReaderState m b i) m)
  => m (Inline i)
inlineP = Inline <$> inlineP'

-- * Paragraphs
syntacticsNoNewlineL :: String
syntacticsNoNewlineL = "%$*_~^`\"\\"

syntactics :: S.Set Char
syntactics = S.fromList ('\n' : syntacticsNoNewlineL)

syntacticsNoNewline :: S.Set Char
syntacticsNoNewline = S.fromList syntacticsNoNewlineL

isSyntactic :: Char -> Bool
isSyntactic = (`elem` syntactics)

escapedP :: MP.MonadParsec e T.Text m => m a -> m a
escapedP p = MP.single '\\' >> p

escapedSyntacticP :: MP.MonadParsec e T.Text m => m T.Text
escapedSyntacticP = T.singleton <$> escapedP (MP.choice $ MP.single <$> syntacticsNoNewlineL)

paragraphStarterP :: MP.MonadParsec e T.Text m => m ()
paragraphStarterP = MP.label "paragraph starter" $ do
  void $ MP.single '\n'
  void $ MP.eitherP (MP.lookAhead $ MP.chunk "\n++") (MP.single '\n')

paragraphContinuerP :: MP.MonadParsec e T.Text m => m ()
paragraphContinuerP = void $ MP.chunk "\n++\n"

textP :: MP.MonadParsec e T.Text m => m (Inline i)
textP = MP.label "text" $ fmap Text $ do
  firstChar <- MP.satisfy (`notElem` ("*_`$^~%" :: String))
  case firstChar of
    '\\' -> T.singleton <$> MP.anySingle
    _    -> T.cons firstChar
      <$> MP.takeWhileP Nothing (`notElem` ("*_`$^~%\n\\" :: String))

paragraphLabelP :: MP.MonadParsec e T.Text m => m Label
paragraphLabelP = blockLabelP

foldText'
  :: (TB.Builder, [ParaContent b i])
  -> ParaContent b i
  -> (TB.Builder, [ParaContent b i])
foldText' (textBuilder, contents) (ParaInline (Text t)) =
  (textBuilder <> TB.fromText t, contents)
foldText' (textBuilder, contents) c =
  let text = TL.toStrict $ TB.toLazyText textBuilder
  in  ( ""
      , c
        : (if T.null text then contents else (ParaInline $ Text text) : contents
          )
      )

foldText :: [ParaContent b i] -> [ParaContent b i]
foldText contents =
  let (endText, endContents) = F.foldl' foldText' ("", []) contents
      text                   = TL.toStrict $ TB.toLazyText endText
  in  reverse $ if T.null text
        then endContents
        else (ParaInline $ Text text) : endContents

paragraphP
  :: (MP.MonadParsec e T.Text m, MonadReader (IDocReaderState m b i) m)
  => m (Paragraph b i)
paragraphP = MP.label "paragraph" $ do
  paragraphStarterP
  mLabel <- optional paragraphLabelP
  let inlineParser = do
        MP.notFollowedBy (MP.choice $ MP.chunk <$> ["\n\n", "\n---", "\n++"])
        inlineP <|> textP
      blockParser = do
        paragraphContinuerP
        block <- blockP'
        void $ optional paragraphContinuerP
        return block
  contents <-
    fmap foldText
    $   some
    $   (ParaBlock <$> blockParser)
    <|> (ParaInline <$> inlineParser)
  return Paragraph { _paraContents = V.fromList contents, _paraLabel = mLabel }

-- * Parser Helpers
parseIDoc
  :: IDocReaderState (IDocParser e b i) b i
  -> IDocParser e b i a
  -> T.Text
  -> T.Text
  -> Either (MP.ParseErrorBundle T.Text e) a
parseIDoc readerState (IDocParser parser) inputName input =
  let ranParser   = MP.runParserT parser (T.unpack inputName) input
      ranReader   = runReaderT ranParser readerState
      ranIdentity = runIdentity ranReader
  in  ranIdentity

-- * The Empty Language
emptyS :: Monad m => IDocReaderState m Identity ()
emptyS = IDocReaderState { _blocksP      = \_ _ _ _ -> return (Identity ())
                         , _sectionsP    = \_ _ _ _ -> return (Identity ())
                         , _inlinesP     = \_ _ _ -> return ()
                         , _blockAttrsP  = return Null
                         , _inlineAttrsP = return Null
                         }
