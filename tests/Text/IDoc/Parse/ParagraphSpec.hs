{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists #-}
module Text.IDoc.Parse.ParagraphSpec where

import Text.IDoc.Syntax
import Text.IDoc.Parse

import Text.Megaparsec hiding (Label)
import Test.Hspec
import Test.QuickCheck
import Data.Void
import Data.Text
import Data.Functor.Identity
import Data.Vector

parsed :: a -> Either (ParseErrorBundle s e) a
parsed = return

parseParagraph :: Text -> Either (ParseErrorBundle Text Void) (Paragraph Identity ())
parseParagraph = parseIDoc emptyS paragraphP "<test-input>"

paragraphSpec :: Spec
paragraphSpec = parallel $ do
  describe "paragraphP" $ do
    it "should parse a paragraph of a single line of text" $ do
      parseParagraph "\n\nHello, world!" `shouldBe` (parsed $ Paragraph [ParaInline $ Text "Hello, world!"] Nothing)
    it "should parse a paragraph which has another one after it" $ do
      parseParagraph "\n\nHello, world!\n\nGoodbye, Jojo!" `shouldBe` (parsed $ Paragraph [ParaInline $ Text "Hello, world!"] Nothing)
    it "should parse multi-line paragraphs" $ do
      parseParagraph "\n\nHello, world!\nGoodbye, Jojo!" `shouldBe` (parsed $ Paragraph [ParaInline $ Text "Hello, world!\nGoodbye, Jojo!"] Nothing)
    it "should parse a paragraph with a label" $ do
      parseParagraph "\n\n<aLabel>\nHello, world!" `shouldBe` (parsed $ Paragraph [ParaInline $ Text "Hello, world!"] (Just $ Label "aLabel"))
    it "should parse a paragraph that is just an inline" $ do
      parseParagraph "\n\n%inline[]" `shouldBe` (parsed $ Paragraph [ParaInline $ Inline ()] Nothing)
    it "should parse a paragraph that starts with a block" $ do
      parseParagraph "\n\n++\n@block\n---\n---\n++\nHello, world!" `shouldBe` (parsed $ Paragraph [ParaBlock $ Identity (), ParaInline $ Text "Hello, world!"] Nothing)
    it "should parse a block-started paragraph that ends with a continuer" $ do
      parseParagraph "\n\n++\n@block\n---\n---\n++\n\n\n" `shouldBe` (parsed $ Paragraph [ParaBlock $ Identity ()] Nothing)
