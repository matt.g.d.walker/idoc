{-# LANGUAGE OverloadedStrings #-}
module Text.IDoc.Parse.BlockSpec where

import Text.IDoc.Syntax
import Text.IDoc.Parse

import Text.Megaparsec
import Test.Hspec
import Test.QuickCheck
import Data.Void
import Data.Text
import Data.Functor.Identity

parsed :: a -> Either (ParseErrorBundle s e) a
parsed = return

parseEmptyBlock :: Text -> Either (ParseErrorBundle Text Void) (Block Identity ())
parseEmptyBlock = parseIDoc emptyS blockP "<test-input>"

shouldBeEmpty s = (parseEmptyBlock s `shouldBe` (parsed $ Block $ Identity ()))

blockSpec :: Spec
blockSpec = parallel $ do
  describe "blockP" $ do
    it "should parse the simplest empty block" $ do
      shouldBeEmpty "@block\n---\n---"
    it "should parse an empty block with just a label" $ do
      shouldBeEmpty "@block\n<label>\n---\n---"
    it "should parse an empty block with just a title" $ do
      shouldBeEmpty "@block\n#Title\n---\n---"
    it "should parse an empty block with just empty attrs" $ do
      shouldBeEmpty "@block\n...\n---\n---"
    it "should parse an empty block with label and title" $ do
      shouldBeEmpty "@block\n<label>\n#Title\n---\n---"  
    it "should parse an empty block with label and attrs" $ do
      shouldBeEmpty "@block\n<label>\n...\n---\n---"  
    it "should parse an empty block with title and attrs" $ do
      shouldBeEmpty "@block\n#Title\n...\n---\n---"  
    it "should parse an empty block with everything" $ do
      shouldBeEmpty "@block\n<label>\n#Title\n...\n---\n---"